package com.sourceit.hackathon.databaseexample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.sourceit.hackathon.databaseexample.DataBaseCreator.User.MAIN_USER;
import static com.sourceit.hackathon.databaseexample.DataBaseCreator.User.TABLE_NAME;
import static com.sourceit.hackathon.databaseexample.DataBaseCreator.User.USER_AGE;

/**
 * Created by wenceslaus on 10.06.17.
 */

public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database==null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance==null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }


    public long insertUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(MAIN_USER, user.name);
        cv.put(USER_AGE, user.age);
        return database.insert(TABLE_NAME, null, cv);
    }

    public List<User> getUsers() {
        String query = " SELECT " +
//                " * " +
                MAIN_USER + ", " + USER_AGE +
                " FROM " + TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        List<User> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();
            user.name = cursor.getString(cursor.getColumnIndex(MAIN_USER));
            user.age = cursor.getInt(cursor.getColumnIndex(USER_AGE));
            list.add(user);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


}
